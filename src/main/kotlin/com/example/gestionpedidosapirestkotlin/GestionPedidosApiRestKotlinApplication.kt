package com.example.gestionpedidosapirestkotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GestionPedidosApiRestKotlinApplication

fun main(args: Array<String>) {
    runApplication<GestionPedidosApiRestKotlinApplication>(*args)
}
